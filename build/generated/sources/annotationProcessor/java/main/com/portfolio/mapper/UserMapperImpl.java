package com.portfolio.mapper;

import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.entity.User;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-01-13T12:56:04+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 15 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public RegistrationDTO modelToDto(User user) {
        if ( user == null ) {
            return null;
        }

        RegistrationDTO registrationDTO = new RegistrationDTO();

        registrationDTO.setName( user.getName() );
        registrationDTO.setBirthDate( user.getBirthDate() );
        registrationDTO.setEmail( user.getEmail() );
        registrationDTO.setUsername( user.getUsername() );
        registrationDTO.setPassword( user.getPassword() );
        registrationDTO.setJob( user.getJob() );

        return registrationDTO;
    }

    @Override
    public User dtoToModel(RegistrationDTO registrationDTO) {
        if ( registrationDTO == null ) {
            return null;
        }

        User user = new User();

        user.setUsername( registrationDTO.getUsername() );
        user.setPassword( registrationDTO.getPassword() );
        user.setName( registrationDTO.getName() );
        user.setBirthDate( registrationDTO.getBirthDate() );
        user.setEmail( registrationDTO.getEmail() );
        user.setJob( registrationDTO.getJob() );

        return user;
    }

    @Override
    public User dtoToModel(LoginDTO userDTO) {
        if ( userDTO == null ) {
            return null;
        }

        User user = new User();

        user.setUsername( userDTO.getUsername() );
        user.setPassword( userDTO.getPassword() );

        return user;
    }

    @Override
    public User dtoToModel(RegistrationDTO registrationDTO, User user) {
        if ( registrationDTO == null ) {
            return null;
        }

        user.setUsername( registrationDTO.getUsername() );
        user.setPassword( registrationDTO.getPassword() );
        user.setName( registrationDTO.getName() );
        user.setBirthDate( registrationDTO.getBirthDate() );
        user.setEmail( registrationDTO.getEmail() );
        user.setJob( registrationDTO.getJob() );

        return user;
    }
}
