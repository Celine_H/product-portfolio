package com.portfolio.service.impl;

import com.portfolio.entity.Product;
import com.portfolio.repository.ProductRepository;
import com.portfolio.service.ProductService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductServiceImpl implements ProductService {

    final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository){
        this.productRepository=productRepository;
    }

    @Override
    public List<Product> findAllProducts(String username) {
        return productRepository.findProductsByUsername(username);
    }

    @Override
    public Optional<Product> findProduct(String username, String productCode) {
        return findAllProducts(username).stream().filter(p->p.getProductCode().equals(productCode)).findAny();
    }

    @Override
    public Product insertProduct(String username, String productCode) {
     return   productRepository.addOrder(username,productCode);
    }

    @Override
    public boolean deleteProduct(String username, String productCode) {
       return productRepository.deleteOrder(username,productCode);
    }

    @Override
    public boolean deleteAllProducts(String username) {
       return productRepository.deleteAllOrders(username);
    }


}
