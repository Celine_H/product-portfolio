package com.portfolio.service.impl;

import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.encoder.PasswordEncoder;
import com.portfolio.entity.User;
import com.portfolio.enums.ExceptionEnum;
import com.portfolio.exception.UserAlreadyExistsException;
import com.portfolio.exception.WrongCredentialsException;
import com.portfolio.jwt.JwtUtil;
import com.portfolio.mapper.UserMapper;
import com.portfolio.repository.UserRepository;
import com.portfolio.service.AuthService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Log4j2
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthServiceImpl implements AuthService {


    final UserRepository userRepository;
    final JwtUtil jwtUtil;
    final UserMapper userMapper;

    @Override
    public String login(LoginDTO loginDTO) {
        User user = userRepository.findByUsername(loginDTO.getUsername()).orElseThrow(WrongCredentialsException::new);
        if (!user.getPassword().equals(PasswordEncoder.sha256(loginDTO.getPassword()))) {
            throw new WrongCredentialsException();
        }
        return jwtUtil.generate(user.getUsername(), user.getPassword());
    }


    @Override
    public RegistrationDTO register(RegistrationDTO registrationDTO) throws MessagingException {
        userRepository.findByUsername(registrationDTO.getUsername()).ifPresent(ex -> {
            throw new UserAlreadyExistsException(ExceptionEnum.USER_ALREADY_EXISTS);
        });
        registrationDTO.setPassword(PasswordEncoder.sha256(registrationDTO.getPassword()));
        User user = userRepository.save(userMapper.dtoToModel(registrationDTO));
        return userMapper.modelToDto(user);
    }




}
