package com.portfolio.service;

import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;

import javax.mail.MessagingException;

public interface AuthService {
    String login(LoginDTO loginDTO);

    RegistrationDTO register(RegistrationDTO registrationDTO) throws MessagingException;
}
