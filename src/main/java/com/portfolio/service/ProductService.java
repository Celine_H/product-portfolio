package com.portfolio.service;


import com.portfolio.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> findAllProducts(String username);

    Optional<Product> findProduct(String username, String productCode);

    Product insertProduct(String username, String productCode);

    boolean deleteProduct(String username, String productCode);

    boolean deleteAllProducts(String username);

}
