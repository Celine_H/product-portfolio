package com.portfolio.config;

import com.portfolio.entity.Product;
import com.portfolio.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
public class AutoRun implements CommandLineRunner {


    ProductRepository productRepository;

    public AutoRun(ProductRepository productRepository){
        this.productRepository=productRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        productRepository.save (new Product("abc123","airpods", BigDecimal.valueOf(350.00)));
        productRepository.save (new Product("abc124","remote-mouse",BigDecimal.valueOf(99.99)));
        productRepository.save (new Product("ebc456","dvd-player",BigDecimal.valueOf(250.00)));
        productRepository.save (new Product("fde567","usb",BigDecimal.valueOf(30.00)));
        productRepository.save (new Product("hij789","printer",BigDecimal.valueOf(450.99)));
    }
}
