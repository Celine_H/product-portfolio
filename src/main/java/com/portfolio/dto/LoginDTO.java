package com.portfolio.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginDTO {
    @NotBlank(message = "username_is_required")
    String username;
    @NotBlank(message = "password_is_required")
    @Size(min = 8, message = "password_length_shouldn't_be_less_than_8")
    String password;
}
