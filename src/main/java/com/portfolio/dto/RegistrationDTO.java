package com.portfolio.dto;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegistrationDTO {

    @NotBlank(message = "name_is_required")
    String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate birthDate;
    @NotBlank(message = "email_is_required")
    String email;
    @NotBlank(message = "username_is_required")
    String username;
    @NotBlank(message = "password_is_required")
    @Size(min = 8, message = "password_length_shouldn't_be_less_than_8")
    String password;
    @NotBlank(message = "job_is_required")
    String job;

}
