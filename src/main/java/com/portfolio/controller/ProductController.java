package com.portfolio.controller;

import com.portfolio.entity.Product;
import com.portfolio.jwt.JwtUser;
import com.portfolio.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping("/products")
public class ProductController {

    final ProductService productService;


    @GetMapping
    public ResponseEntity<List<Product>> findProducts(Authentication auth){
        JwtUser jwtUser = (JwtUser) auth.getPrincipal();
        return ResponseEntity.ok(productService.findAllProducts(jwtUser.getUsername()));
    }

    @GetMapping("/{productCode}")
    public ResponseEntity<Product> findProductByCode(@PathVariable String productCode, Authentication auth){
        JwtUser jwtUser = (JwtUser) auth.getPrincipal();
        return ResponseEntity.of(productService.findProduct(jwtUser.getUsername(),productCode));
    }

    @PostMapping("/{productCode}")
    public ResponseEntity<Product> save(@PathVariable String productCode, Authentication auth){
        JwtUser jwtUser = (JwtUser) auth.getPrincipal();
        Product product = productService.insertProduct(jwtUser.getUsername(), productCode);
        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

    @DeleteMapping("/{productCode}")
    public ResponseEntity<Product> deleteProduct(@PathVariable String productCode,Authentication auth){
        JwtUser jwtUser = (JwtUser) auth.getPrincipal();
        if(productService.deleteProduct(jwtUser.getUsername(),productCode)){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/deleteAll")
    public ResponseEntity<Product> deleteAllProducts(Authentication auth){
        JwtUser jwtUser = (JwtUser) auth.getPrincipal();
        if( productService.deleteAllProducts(jwtUser.getUsername())){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
