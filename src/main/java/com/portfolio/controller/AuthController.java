package com.portfolio.controller;


import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.service.AuthService;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequestMapping("/user")
public class AuthController {

    final AuthService authService;

    @PostMapping("login")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginDTO, String ip) {
        return ResponseEntity.ok(authService.login(loginDTO));
    }

    @PostMapping("registration")
    public ResponseEntity<RegistrationDTO> register(@Valid @RequestBody RegistrationDTO registrationDTO) throws MessagingException {
        return ResponseEntity.ok(authService.register(registrationDTO));
    }

}
