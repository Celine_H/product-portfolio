package com.portfolio.entity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Table(name = "users_table")
public class User{

    @Id
     String username;
     String password;
     String name;
     LocalDate birthDate;
     String email;
     String job;
    @ManyToMany(mappedBy = "users", cascade = CascadeType.ALL)
     List<Product> products;
}
