package com.portfolio.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name="products_table")
public class Product {

    @Id
    @Column(name="product_code")
    private String productCode;
    private String productName;
    private BigDecimal price;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "portfolio_table",
            joinColumns = {@JoinColumn(name = "product_code", referencedColumnName ="product_code" )},
            inverseJoinColumns = {@JoinColumn(name = "username", referencedColumnName = "username")}
    )
    private Set<User> users;

    public Product(String productCode, String productName, BigDecimal price) {
        this.productCode=productCode;
        this.productName=productName;
        this.price=price;
    }
}
