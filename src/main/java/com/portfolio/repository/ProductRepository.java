package com.portfolio.repository;


import com.portfolio.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,String> {

    Optional<Product> findProductByProductCode(String code);


    @Query(value = "SELECT * FROM order WHERE order.username=:username", nativeQuery = true)
    List<Product> findProductsByUsername(@Param("username") String username);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO order(username,product_code) VALUES(:username,:productCode)", nativeQuery = true)
    Product addOrder(String username,String productCode);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM order WHERE order.username=:username AND order.product_code=:productCode", nativeQuery = true)
    boolean deleteOrder(@Param("username") String username, @Param("productCode") String productCode);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM order WHERE order.username=:username ", nativeQuery = true)
    boolean deleteAllOrders(@Param("username") String username);

}
