package com.portfolio.mapper;

import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {

    RegistrationDTO modelToDto(User user);

    User dtoToModel(RegistrationDTO registrationDTO);


    User dtoToModel(LoginDTO userDTO);


    User dtoToModel(RegistrationDTO registrationDTO, @MappingTarget User user);
}
