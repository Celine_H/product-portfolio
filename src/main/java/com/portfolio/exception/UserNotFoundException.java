package com.portfolio.exception;

import com.portfolio.enums.ExceptionEnum;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserNotFoundException extends UserBaseException {

    public UserNotFoundException() {
        super(ExceptionEnum.USER_NOT_FOUND.getCode(), ExceptionEnum.USER_NOT_FOUND.name().toLowerCase());
    }
}
