package com.portfolio.exception.handler;

import com.portfolio.dto.ErrorDTO;
import com.portfolio.enums.ExceptionEnum;
import com.portfolio.exception.UserAlreadyExistsException;
import com.portfolio.exception.UserNotFoundException;
import com.portfolio.exception.WrongCredentialsException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.Objects;

@Log4j2
@RestControllerAdvice
public class ExceptionHandler {


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(UserNotFoundException.class)
    public ErrorDTO handleException(UserNotFoundException ex){
        log.error ("User not found exception ");
        return ErrorDTO.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .dateTime(ex.getDateTime())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(UserAlreadyExistsException.class)
    public ErrorDTO handleException(UserAlreadyExistsException ex){
        log.error ("User already exists exception");
        return ErrorDTO.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .dateTime(ex.getDateTime())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler({MethodArgumentNotValidException.class})
    public Object handleException(MethodArgumentNotValidException ex) {
        log.error ("Method argument not valid exception");
        FieldError fieldError = ex.getBindingResult().getFieldError();
        return ErrorDTO
                .builder()
                .code(ExceptionEnum.USER_VALIDATION_ERROR.getCode())
                .message(Objects.requireNonNull(fieldError).getDefaultMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(WrongCredentialsException.class)
    public ErrorDTO handleException(WrongCredentialsException ex){
        log.error("Error occurred while processing request.");
        return ErrorDTO.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .dateTime(ex.getDateTime())
                .build();
    }



    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @org.springframework.web.bind.annotation.ExceptionHandler(Throwable.class)
    public ErrorDTO handleException(Throwable ex){
        log.error ("{} error occurred while processing request.", ex.getMessage());
        return ErrorDTO.builder()
                .code(ExceptionEnum.SERVER_ERROR.getCode())
                .message(ex.getMessage())
                .dateTime(LocalDateTime.now())
                .build();
    }
}
