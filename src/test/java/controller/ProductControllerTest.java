package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.controller.ProductController;
import com.portfolio.dto.LoginDTO;
import com.portfolio.entity.Product;
import com.portfolio.jwt.JwtUser;
import com.portfolio.service.ProductService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
 class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ProductService proService;

    @MockBean
    Authentication auth;

    @MockBean
   LoginDTO loginDTO;

     static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
     void testSaveProduct() throws Exception {

        Product product = new Product("111lll", "radio", BigDecimal.valueOf(123.45));
        when(proService.insertProduct(anyString(), anyString())).thenReturn(product);

        mockMvc.perform(post("api/products/111lll").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(asJsonString(product))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect((ResultMatcher) content().contentType("application/json;charset=UTF-8"))
                .andExpect((ResultMatcher) jsonPath("$.productCode", Matchers.equalTo("111lll")));

    }

    @Test
     void testGetProduct() throws Exception {

        BigDecimal price = BigDecimal.valueOf(350.00);

        Product product = new Product("abc123","airpods",BigDecimal.valueOf(350.00));
        when(proService.findProduct(anyString(),anyString())).thenReturn(java.util.Optional.of(product));

        RequestBuilder requestBuilder= get("api/products/abc123").accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult=  mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productName", Matchers.equalTo("airpods")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price",Matchers.equalTo(price))).andReturn();

    }


    @Test
     void testDeleteProduct() throws Exception {
        when(proService.deleteProduct(anyString(),anyString())).thenReturn(true);
        RequestBuilder requestBuilder= delete("api/products/abc124").accept(MediaType.APPLICATION_JSON);
        MvcResult requestResult = mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
    }

    @Test
     void testDeleteAllProducts() throws Exception {
        when(proService.deleteAllProducts(anyString())).thenReturn(true);
        RequestBuilder requestBuilder= delete("api/products/deleteAll").accept(MediaType.APPLICATION_JSON);
        MvcResult requestResult = mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
    }

    @Test
     void testGetAllProducts() throws Exception {

        Product product = new Product("112bdf", "stereo", BigDecimal.valueOf(125.50));
        when(proService.findAllProducts(anyString())).thenReturn(Arrays.asList(product));

        RequestBuilder requestBuilder= get("api/products").accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult=  mockMvc.perform(requestBuilder).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].productCode", Matchers.equalTo("112bdf"))).andReturn();

    }

}
