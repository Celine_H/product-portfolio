package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.controller.ProductController;
import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.entity.Product;
import com.portfolio.jwt.JwtUtil;
import com.portfolio.mapper.UserMapper;
import com.portfolio.repository.UserRepository;
import com.portfolio.service.AuthService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.mail.MessagingException;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
public class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    LoginDTO loginDTO;

    @MockBean
    RegistrationDTO registrationDTO;

    @MockBean
    AuthService authService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    JwtUtil jwtUtil;

    @MockBean
    UserMapper userMapper;

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void login() throws Exception {

    LoginDTO loginDTO=LoginDTO.builder().username("user0044").password("password12").build();

    when(authService.login(Mockito.any(LoginDTO.class))).thenReturn("user0044","password12");

    RequestBuilder requestBuilder= post("api/user/login").accept(MediaType.APPLICATION_JSON);

    mockMvc.perform(requestBuilder).andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.username",Matchers.equalTo("user0044")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.password",Matchers.equalTo("password12")));


    }

    @Test
    public void registration() throws Exception {
        RegistrationDTO registrationDTO=RegistrationDTO.builder()
                .birthDate(LocalDate.parse("1998-01-01"))
                .email("abc@gmail.com")
                .job("developer")
                .password("password12")
                .username("user0044")
                .name("jimmy carter").build();

        when(authService.register(Mockito.any(RegistrationDTO.class))).thenReturn(registrationDTO);

        RequestBuilder requestBuilder= post("api/user/registration").accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();




    }

}
