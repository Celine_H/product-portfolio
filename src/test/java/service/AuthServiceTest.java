package service;

import com.portfolio.dto.LoginDTO;
import com.portfolio.dto.RegistrationDTO;
import com.portfolio.entity.User;
import com.portfolio.exception.UserAlreadyExistsException;
import com.portfolio.exception.WrongCredentialsException;
import com.portfolio.jwt.JwtUtil;
import com.portfolio.mapper.UserMapper;
import com.portfolio.repository.ProductRepository;
import com.portfolio.repository.UserRepository;
import com.portfolio.service.impl.AuthServiceImpl;
import com.portfolio.service.impl.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {


    @InjectMocks
    AuthServiceImpl authService;

    @Mock
    UserRepository userRepository;

    @Mock
    JwtUtil jwtUtil;

    @Mock
    UserMapper userMapper;

    User user1= User.builder().birthDate(LocalDate.parse("1998-01-01"))
            .email("abc@gmail.com")
            .job("developer")
            .password("password12")
            .username("user0044")
            .name("jimmy carter").build();
    RegistrationDTO registerDTO=RegistrationDTO.builder().username("user0044")
            .birthDate(LocalDate.parse("1998-01-01"))
            .email("abc@gmail.com")
            .job("developer")
            .password("password12")
            .name("jimmy carter").build();

    LoginDTO loginDTO1=LoginDTO.builder().username("user0044").password("password12").build();
    LoginDTO loginDTO2=LoginDTO.builder().username("user0040").password("password12").build();

    @Before
    public void setup() {
        userRepository.save(user1);
    }

    @Test
    public void givenUserExistsThrowException(){
        final UserAlreadyExistsException exception = assertThrows(UserAlreadyExistsException.class,
                () -> authService.register(registerDTO));
        assertEquals("User already exists exception", exception.getMessage());
    }

    @Test
    public void givenUserDoesNotExistRegister() throws MessagingException {
        when(userRepository.save(userMapper.dtoToModel(registerDTO))).thenReturn(user1);
        RegistrationDTO dto = authService.register(registerDTO);
        assertEquals(userMapper.dtoToModel(registerDTO),user1);
    }

    @Test
    public void whenWrongCredentialsThrowException(){
        final WrongCredentialsException exception=assertThrows(WrongCredentialsException.class,()-> authService.login(loginDTO2));
        assertEquals("Error occurred while processing request.", exception.getMessage());
    }
    @Test
    public void whenRegisteredUserLogsLogin(){
        String generated = jwtUtil.generate("user0044", "password12");
        when(authService.login(loginDTO1)).thenReturn(generated);
        assertEquals(generated,authService.login(loginDTO1));
    }

}
