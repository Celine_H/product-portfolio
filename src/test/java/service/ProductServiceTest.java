package service;

import com.portfolio.entity.Product;
import com.portfolio.entity.User;
import com.portfolio.repository.ProductRepository;
import com.portfolio.repository.UserRepository;
import com.portfolio.service.impl.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    @Mock
    UserRepository userRepository;

    User user1= User.builder().birthDate(LocalDate.parse("1998-01-01"))
            .email("abc@gmail.com")
            .job("developer")
            .password("password12")
            .username("user0044")
            .name("jimmy carter").build();

    Product product1=Product.builder().productCode("abc123").productName("airpods").price(BigDecimal.valueOf(350.00)).build();

    @Before
    public void setup() {
       userRepository.save(user1);
       productRepository.addOrder("user0044","abc123");
    }

    @Test
    public void whenSavingProductReturnProduct(){
        Product product2=Product.builder().productCode("ebc456").productName("dvd-player").price(BigDecimal.valueOf(250.00)).build();
        when(productRepository.addOrder("john","abc123")).thenReturn(product2);
        Product expectedProduct = productService.insertProduct("john", "ebc456");
        Assertions.assertEquals(expectedProduct,product2);

    }
    @Test
    public void givenProductsExistsReturnListOfProducts(){
        Product product2=Product.builder().productCode("ebc456").productName("dvd-player").price(BigDecimal.valueOf(250.00)).build();
        when(productRepository.addOrder("john","ebc456")).thenReturn(product2);

        List<Product> products = productService.findAllProducts("john");
        List<Product> expectedProducts = Arrays.asList(product1, product2);

        assertThat(expectedProducts).hasSize(products.size()).hasSameElementsAs(products);
    }

    @Test
    public void givenProductsDoNotExistReturnEmptyList(){
        when(productRepository.deleteOrder("user0044","abc123")).thenReturn(true);
        List<Product> products = productService.findAllProducts("user0044");
        assertThat(products).isEmpty();
    }

    @Test
    public void givenSuchProductCodeExistReturnProduct(){
        when(productRepository.findProductByProductCode("abc123")).thenReturn(Optional.ofNullable(product1));
        Product expectedProduct = productService.findProduct("john", "abc123").get();
        Assertions.assertEquals(expectedProduct,product1);

    }


    @Test
    public void givenSuchProductCodeDoesNotExistReturnEmpty(){
        Product product3=Product.builder().productCode("abc123").productName("airpods").price(BigDecimal.valueOf(350.00)).build();
        when(productRepository.findProductByProductCode("klmklm")).thenReturn(Optional.of(product3));
        Optional<Product> expectedProduct = productService.findProduct("john", "klmklm");
        assertThat(expectedProduct).isEmpty();

    }

    @Test
    public void givenProductExistsDeleteProduct(){
        productService.deleteProduct("user0044","abc123");
        verify(productRepository, times(1)).deleteOrder("john","abc123");
    }

    @Test
    public void givenProductDoesNotExistsDoNothing(){
        productService.deleteProduct("user0044","abc123");
        verify(productRepository, times(0)).deleteOrder("john","abc123");
    }

    @Test
    public void givenUserHasProductsDeleteProducts(){
        Product product2=Product.builder().productCode("ebc456").productName("dvd-player").price(BigDecimal.valueOf(250.00)).build();
        when(productRepository.addOrder("john","ebc456")).thenReturn(product2);
        productService.deleteAllProducts("user0044");
        verify(productRepository, times(2)).deleteAllOrders("user0044");
    }

    @Test
    public void givenUserHasNoProductsDoNothing(){
        productRepository.deleteOrder("user0044","abc123");
        productService.deleteAllProducts("user0044");
        verify(productRepository, times(0)).deleteAllOrders("user0044");
    }



}
